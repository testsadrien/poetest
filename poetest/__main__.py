#!/usr/bin/env python3

"""
File needed to setup executable module.
Contains only main function.
Can be launch by command : python -m <module name>
"""

from poetest import kata


def main_function():
    for i in range(1, 106):
        print(kata.fusroh(i))


main_function()
